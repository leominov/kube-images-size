package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"sort"
	"strings"

	"github.com/dustin/go-humanize"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var (
	imageFilter = flag.String("image", "qlean-242314", "Filter by image name")
)

func realMain() error {
	clientset, err := NewClientSet()
	if err != nil {
		return err
	}

	nodeList, err := clientset.CoreV1().Nodes().List(context.Background(), metav1.ListOptions{})
	if err != nil {
		return err
	}

	for _, node := range nodeList.Items {
		var images []string
		fmt.Printf("%s:\n", node.Name)
		for _, image := range node.Status.Images {
			name := ImageNameWithoutSha256(image)
			if *imageFilter != "" && !strings.Contains(name, *imageFilter) {
				continue
			}
			images = append(images, fmt.Sprintf("%s: %s", name, humanize.Bytes(uint64(image.SizeBytes))))
		}
		if len(images) == 0 {
			fmt.Println("none")
			continue
		}
		sort.Strings(images)
		fmt.Println(strings.Join(images, "\n"))
	}

	return nil
}

func main() {
	flag.Parse()
	err := realMain()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
